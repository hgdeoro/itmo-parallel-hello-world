cmake_minimum_required(VERSION 3.17)
project(holamundo C)

set(CMAKE_C_STANDARD 99)

add_executable(holamundo main.c)

find_package(OpenMP REQUIRED)
target_link_libraries(holamundo PRIVATE OpenMP::OpenMP_C)
